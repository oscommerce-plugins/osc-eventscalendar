# eventscalendar

Events Calendar for OS Commerce: This was created for a 2009 version and may no longer work.

## About

Events Calendar allows you to add items to your catalog that will expire at a 
specific date and time.

This is a fork of Expired Products 1.3c(http://addons.oscommerce.com/info/6980).
Events Calendar is geared more towards setting up events and is much more
flexable for that purpose. If you are using Expired Products to add events to
your catalog, it is highly recomended that you switch over to Events Calendar
as Expired Products will no longer be actively maintained.

For more information visit the plugin page at http://addons.oscommerce.com/info/7071

## Install

1. Download the latest version https://bitbucket.org/matthewlinton/osc_eventscalendar/downloads
1. Unzip the archive
1. change into the eventscalendar directory 'cd OSC_EventsCalendar'
1. Read the instructions in 'events_calendar.txt'

## OS Commerce

http://www.oscommerce.com/

OS Commerce is a complete framework for creating your very own online store.

## PHP Calendar

PHP Calendar (version 2.3), written by Keith Devens

http://keithdevens.com/software/php_calendar

  see example at http://keithdevens.com/weblog

License: http://keithdevens.com/software/license

## Date Time Picker

Date Time Picker Copyright (c) 2003 TengYong Ng

TengYong Ng

contact@rainforestnet.com

http://www.rainforestnet.com
